#include <stdio.h>
#include <string.h>

void removeStdChar(char array[]) {
    if (strchr(array, '\n') == NULL) {
        while (fgetc(stdin) != '\n');
    }
}

int main() {
    char s1[255];
    printf("Enter string:");
    fgets(s1, sizeof(s1) * sizeof(char), stdin);
    removeStdChar(s1);
    printf("Number of characters:\n");
    int noOfA, noOfE, noOfI, noOfO, noOfU, noOfConsonants;
    noOfA = 0;
    noOfE = 0;
    noOfI = 0;
    noOfO = 0;
    noOfU = 0;
    noOfConsonants = 0;
    for (int i = 0; i < strlen(s1); ++i) {
        if (s1[i] == 'a' || s1[i] == 'A') {
            noOfA++;
        } else if (s1[i] == 'e' || s1[i] == 'E') {
            noOfE++;
        } else if (s1[i] == 'i' || s1[i] == 'I') {
            noOfI++;
        } else if (s1[i] == 'o' || s1[i] == 'O') {
            noOfO++;
        } else if (s1[i] == 'u' || s1[i] == 'U') {
            noOfU++;
        } else if ((s1[i] >= 'A' && s1[i] <= 'Z')||(s1[i] >= 'a' && s1[i] <= 'z')) {
            noOfConsonants++;
        }
    }
    printf("Number of character:\n");
    printf("a: %d; e: %d; i: %d; o: %d; u: %d; rest: %d\n", noOfA, noOfE, noOfI, noOfO, noOfU, noOfConsonants);
    printf("Percentage of total:\n");
    printf("a: %.2f%%; e: %.2f%%; i: %.2f%%; o: %.2f%%; u: %.2f%%; rest: %.2f%%", (float) noOfA/(strlen(s1)-1)*100, (float)noOfE/(strlen(s1)-1)*100, (float)noOfI/(strlen(s1)-1)*100, (float)noOfO/(strlen(s1)-1)*100, (float)noOfU/(strlen(s1)-1)*100, (float)noOfConsonants/(strlen(s1)-1)*100);
return 0;
}